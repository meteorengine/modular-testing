﻿namespace MeteorEngine.Core
{
    public struct CoreParameters
    {
        public bool EditorMode;
        public bool GameMode;
        public string TemporaryFolderName;
    }
}
