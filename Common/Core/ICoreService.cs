﻿using System;

namespace MeteorEngine.Core
{
    /// <summary>
    /// Core service interface
    /// </summary>
    public interface ICoreService
    {
        void InitializeCore(MyGame GameClass, CoreParameters Parameters, Action OnFinalPrepare);
    }
}
