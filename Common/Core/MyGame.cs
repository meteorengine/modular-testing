﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interop;

namespace MeteorEngine.Core
{
    public class MyGame
    {
        private string[] _startArgs;
        private double _elapsedTime;
        private int _totalFrames;
        private int _minFps;
        private int _maxPfs;
        private long _trianglesDrawn;
        private TimeSpan _maximumElapsedTime;
        private TimeSpan _accumulatedElapsedGameTime;
        private TimeSpan _lastFrameElapsedGameTime;
        private int _nextLastUpdateCountIndex;
        private bool _suppressDraw;
        private int[] _lastUpdateCount;
        private float _updateCountAverageSlowLimit;
        private TimerTick _timer;
        protected int _physicsLockers;
        protected object _physicsLocker;
        public ICoreService Core;
        public IGraphicsDevice Device;
        public DebugEventCPU UpdateTime;
        public DebugEventCPU PluginsTime;
        public DebugEventCPU PhysicsTime;
        public bool IsEverySecondFrame;
        public bool IsEveryThirdFrame;
        public bool IsEveryFourthFrame;
        public bool TakeScreenShotFlag;
        public bool IsRunningSlowly
        {
            get;
            private set;
        }

        public TimeSpan TotalGameTime
        {
            get;
            private set;
        }

        public TimeSpan ElapsedGameTime
        {
            get;
            private set;
        }

        public int FrameCount
        {
            get;
            internal set;
        }

        public int MinFPS
        {
            get
            {
                return _minFps;
            }
        }

        public int MaxFPS
        {
            get
            {
                return _maxPfs;
            }
        }

        public bool IsActive
        {
            get
            {
                return !Window.IsMinimized;
            }
        }

        public bool IsFocused
        {
            get
            {
                return Window.Focused;
            }
        }

        public bool IsFixedTimeStep
        {
            get;
            set;
        }

        public bool IsMouseVisible
        {
            get
            {
                return Window.IsMouseVisible;
            }
            set
            {
                Window.IsMouseVisible = value;
            }
        }

        public bool IsRunning
        {
            get;
            private set;
        }

        public TimeSpan TargetElapsedTime
        {
            get;
            private set;
        }

        public Window Window
        {
            get
            {
                return Device.Window;
            }
        }

        public int FPS
        {
            get;
            private set;
        }

        public long TrianglesPerSecond
        {
            get;
            private set;
        }

        public string[] StartingArgs
        {
            get
            {
                return _startArgs;
            }
        }

        public bool IsPhysicsUnlocked
        {
            get
            {
                return _physicsLockers == 0;
            }
        }

        protected MyGame(int targetFPS, string[] args)
        {
        }

        public void SetTargetFPS(int targetFPS)
        {
            if(targetFPS <= 0)
            {
                IsFixedTimeStep = false;
            }
            IsFixedTimeStep = true;
            _maximumElapsedTime = TimeSpan.FromMilliseconds(500);
            TargetElapedTime = TimeSpan.FromTicks(10000000 / targetFPS);
            _lastUpdateCount = new int[4];
            _nextLastUpdateCountIndex = 0;
            _updateCountAverageSlowLimit = (2 * Math.Min(2, _lastUpdateCount.Length)) + (_lastUpdateCount.Length - 2 * Math.Min(2, _lastUpdateCount.Length)) / _lastUpdateCount.Length;
        }

        /// <summary>
        /// Creates D3DX Device and native window
        /// </summary>
        protected void CreateDevice()
        {
            CreateDevice(null, null);
        }


        /// <summary>
        /// Creates D3DX Device for existing D3DImage
        /// </summary>
        protected void CreateDevice(D3DImage image, Control container)
        {
            //Services.Rendering.CreateDevice(RendererType.DirectX_11, image, container);
        }
    }
}
