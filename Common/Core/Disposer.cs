﻿using System;

namespace MeteorEngine.Core
{
    public static class Disposer
    {
        public static void Dispose<T>(ref T obj) where T : class
        {
            if (obj != null)
            {
                IDisposable disposable = obj as IDisposable;
                if (disposable != null)
                {
                    try
                    {
                        disposable.Dispose();
                    }
                    catch
                    {
                    }
                }
                obj = default(T);
            }
        }
    }
}
