﻿using System;

namespace MeteorEngine
{
    public sealed class DataLeakException : Exception
    {
        private string _customMsg;

        public override string Message => _customMsg == null ? "Data leak." : _customMsg;

        public DataLeakException(){ }

        public DataLeakException(string message)
        {
            _customMsg = message;
        }
    }
}
