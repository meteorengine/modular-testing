﻿using System;

namespace MeteorEngine
{
    public class UnhandledExceptionHandler
    {
        private static void HandleUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            var exceptionObject = args.ExceptionObject as Exception;
            if (exceptionObject != null)
                PrintException("Unhandled Exception: ", exceptionObject);
            //TODO: Handle the exception (Replace logging to console with crash reporter)
        }


        private static void PrintException(string title, Exception e)
        {
            Debug.LogError(string.Concat(title, e.ToString()));
            if (e.InnerException != null)
                PrintException("Inner Exception: ", e.InnerException);
        }

        public static void RegisterUECatcher()
        {
            AppDomain.CurrentDomain.UnhandledException += HandleUnhandledException;
        }
    }
}
