﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeteorEngine
{
    public interface IInputModule : IDisposable
    {
        bool IsPressingAnyKey { get; }
        bool Initialize();
        bool IsKeyPressed(KeyCode key);
        bool IsKeyPressing(KeyCode key);
        bool IsKeyReleased(KeyCode key);
        bool IsButtonPressed(MouseButtons button);
        bool IsButtonPressing(MouseButtons button);
        bool IsButtonReleased(MouseButtons button);
        bool IsControlPressed();
        bool IsControlPressing();
        bool IsControlReleased();
        bool IsShiftPressed();
        bool IsShiftPressing();
        bool IsShiftReleased();
        bool IsAltPressed();
        bool IsAltPressing();
        bool IsAltReleased();
        void Update();
    }
}
