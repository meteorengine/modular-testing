﻿namespace MeteorEngine
{
    /// <summary>
    /// Interface for objects that are initializable. This concept is useful for objects
    /// that can't be fully initialized in their constructor.
    /// </summary>
    public interface IInitializable
    {
        /// <summary>
        /// Initializes the object
        /// </summary>
        void Initialize();
    }
}
