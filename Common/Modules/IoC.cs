﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Reflection;

namespace MeteorEngine
{
    /// <summary>
    /// Used by the framework to pull instances from an container
    /// </summary>
    public static class IoC
    {
        private static CompositionContainer container;

        static IoC()
        {
            //Create new catalog which combines multiple catalogs
            AggregateCatalog catalog = new AggregateCatalog();
            //Add all parts found in all assemblies in the same directory as the executable
            LoadRecursive(catalog, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            container = new CompositionContainer(catalog);
            container.InitializeAll();
        }

        /// <summary>
        /// Method used for recursively getting all Exported classes
        /// </summary>
        /// <param name="catalog">Catalog containing all Directory Catalogs</param>
        /// <param name="path">Path to start with</param>
        private static void LoadRecursive(AggregateCatalog catalog, string path)
        {
            Queue<string> directories = new Queue<string>();
            directories.Enqueue(path);
            while (directories.Count > 0)
            {
                var directory = directories.Dequeue();
                //Load plugins in this folder
                var directoryCatalog = new DirectoryCatalog(directory);
                catalog.Catalogs.Add(directoryCatalog);

                //Add subDirectories to the queue
                var subDirectories = Directory.GetDirectories(directory);
                foreach (string subDirectory in subDirectories)
                {
                    directories.Enqueue(subDirectory);
                }
            }
        }

        private static void InitializeAll(this CompositionContainer container)
        {
            if (container == null)
                throw new ArgumentNullException("container");

            // Create Import definition which satisfies any Export (so all exports are initialized)
            var importDef = new ImportDefinition(contraint => true, string.Empty, ImportCardinality.ZeroOrMore, true, true);

            try
            {
                // Create everything. This ensures that all objects are constructed before IInitializable
                foreach (Export export in container.GetExports(importDef))
                {
                    // Triggers creation of object (otherwise lazy).
                    object tmp = export.Value;
                }

                // Initialize components that require it. Initialization often can't be done in the constructor,
                //  or even after imports have been satisfied by MEF, since we allow circular dependencies between
                //  components, via the System.Lazy class. IInitializable allows components to defer some operations
                //  until all MEF composition has been completed.
                foreach (IInitializable initializable in container.GetExportedValues<IInitializable>())
                    initializable.Initialize();
            }
            catch (CompositionException ex)
            {
                foreach (var error in ex.Errors)
                    //Debug.LogException(ex);
                throw;
            }
        }


        /// <summary>
        /// Gets an instance from the container.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <param name="contractName">Contract name to look up.</param>
        /// <returns>The resolved instance.</returns>
        public static T Get<T>(string contractName = null)
        {
            return container.GetExportedValue<T>(contractName);
        }

        /// <summary>
        /// Gets all instances of a particular type.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <param name="contractName">Contract name to look up.</param>
        /// <returns>The resolved instances.</returns>
        public static IEnumerable<T> GetAll<T>(string contractName = null)
        {
            return container.GetExportedValues<T>(contractName);
        }
    }
}
