﻿using SharpDX.DirectInput;
using SharpDX.XInput;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace MeteorEngine.Input
{
    internal class InputService : IDisposable
    {
        private Stopwatch timer;
        private DirectInput input;
        private Keyboard keyboard;
        private Mouse mouse;
        private Joystick joystick;
        private Controller xbox;
        private List<KeyCode> pressed = new List<KeyCode>(256);
        private List<KeyCode> released = new List<KeyCode>(256);
        private KeyboardState Current_KeyboardState;
        private MouseState Current_MouseState;
        private JoystickState Current_JoystickState;
        private State Current_XBoxState;
        private bool[] Current_Buttons = new bool[5];
        private KeyboardState Previous_KeyboardState;
        private MouseState Previous_MouseState;
        private JoystickState Previous_JoystickState;
        private State Previous_XBoxState;
        private bool[] Previous_Buttons = new bool[5];

        public DirectInput DirectInput => input;
        public Keyboard Keyboard => keyboard;
        public Mouse Mouse => mouse;
        public Controller XBoxGamePad => xbox;
        public Joystick Joystick => joystick;

        public bool IsPressingAnyKey => Current_KeyboardState.PressedKeys.Count > 0;

        public TimeSpan UpdateTime { get; private set; }

        public List<KeyCode> PressedKeys => pressed;
        public List<KeyCode> ReleasedKeys => released;


        public bool Initialize()
        {
            timer = new Stopwatch();

            try
            {
                //Keyboard and mouse
                input = new DirectInput();
                keyboard = new Keyboard(input);
                mouse = new Mouse(input);

                keyboard.Properties.BufferSize = 128;
                mouse.Properties.BufferSize = 128;
                keyboard.Acquire();
                mouse.Acquire();

                //Joystick

                Guid deviceID = Guid.Empty;
                foreach (DeviceInstance instance in input.GetDevices(SharpDX.DirectInput.DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                {
                    deviceID = instance.InstanceGuid;
                }

                if (deviceID != Guid.Empty)
                {
                    joystick = new Joystick(input, deviceID);

                    //TODO: Log Effects

                    joystick.Properties.BufferSize = 128;
                    joystick.Acquire();
                }

                //XBOX controller
                for (int i = 0; i < 4; i++)
                {
                    Controller controller = new Controller((UserIndex)i);
                    if (controller.IsConnected)
                    {
                        xbox = controller;
                        if (xbox != null)
                        {
                            //TODO: Log capabilities
                        }
                        break;
                    }
                }

                pollEvents();
                return false;
            }
            catch
            {
                return true;
            }
        }

        private void pollEvents()
        {
            Previous_KeyboardState = Current_KeyboardState;
            Previous_MouseState = Current_MouseState;
            Previous_JoystickState = Current_JoystickState;
            Previous_XBoxState = Current_XBoxState;
            try
            {
                keyboard.Poll();
                Current_KeyboardState = keyboard.GetCurrentState();
                mouse.Poll();
                Current_MouseState = mouse.GetCurrentState();
                if (joystick != null)
                {
                    joystick.Poll();
                    Current_JoystickState = joystick.GetCurrentState();
                }
                if (xbox != null && xbox.IsConnected)
                {
                    Current_XBoxState = xbox.GetState();
                }
            }
            catch //(Exception ex)
            {
                //TODO: Log Exception
            }
        }

        public bool IsAltPressed() => PressedKeys.Contains(KeyCode.LeftAlt) || ReleasedKeys.Contains(KeyCode.RightAlt);
        public bool IsAltPressing() => Current_KeyboardState.PressedKeys.Contains((Key)KeyCode.LeftAlt) || Current_KeyboardState.PressedKeys.Contains((Key)KeyCode.RightAlt);
        public bool IsAltReleased() => ReleasedKeys.Contains(KeyCode.LeftAlt) || ReleasedKeys.Contains(KeyCode.RightAlt);

        public bool IsButtonPressed(MouseButtons button) => !Previous_Buttons[(int)button] && Current_Buttons[(int)button];
        public bool IsButtonPressing(MouseButtons button) => Current_Buttons[(int)button];
        public bool IsButtonReleased(MouseButtons button) => Previous_Buttons[(int)button] && !Current_Buttons[(int)button];

        public bool IsControlPressed() => ReleasedKeys.Contains(KeyCode.LeftControl) || ReleasedKeys.Contains(KeyCode.RightControl);
        public bool IsControlPressing() => Current_KeyboardState.PressedKeys.Contains((Key)KeyCode.LeftControl) || Current_KeyboardState.PressedKeys.Contains((Key)KeyCode.RightControl);
        public bool IsControlReleased() => ReleasedKeys.Contains(KeyCode.LeftControl) || ReleasedKeys.Contains(KeyCode.RightControl);

        public bool IsKeyPressed(KeyCode key) => PressedKeys.Contains(key);
        public bool IsKeyPressing(KeyCode key) => Current_KeyboardState.PressedKeys.Contains((Key)key);
        public bool IsKeyReleased(KeyCode key) => ReleasedKeys.Contains(key);

        public bool IsShiftPressed() => PressedKeys.Contains(KeyCode.LeftShift) || ReleasedKeys.Contains(KeyCode.RightShift);
        public bool IsShiftPressing() => Current_KeyboardState.PressedKeys.Contains((Key)KeyCode.LeftShift) || Current_KeyboardState.PressedKeys.Contains((Key)KeyCode.RightShift);
        public bool IsShiftReleased() => ReleasedKeys.Contains(KeyCode.LeftShift) || ReleasedKeys.Contains(KeyCode.RightShift);

        public void Update()
        {
            timer.Restart();
            pollEvents();
            pressed.Clear();
            for (int i = 0; i < 256; i++)
            {
                if (Current_KeyboardState.IsPressed((Key)i) && !Previous_KeyboardState.IsPressed((Key)i))
                {
                    pressed.Add((KeyCode)i);
                }
            }
            released.Clear();
            for (int j = 0; j < 256; j++)
            {
                if (!Current_KeyboardState.IsPressed((Key)j) && Previous_KeyboardState.IsPressed((Key)j))
                {
                    released.Add((KeyCode)j);
                }
            }
            for (int k = 0; k < 5; k++)
            {
                Previous_Buttons[k] = Previous_MouseState.Buttons[k];
                Current_Buttons[k] = Current_MouseState.Buttons[k];
            }
            timer.Stop();
            UpdateTime = timer.Elapsed;
        }

        public void Dispose()
        {
            Disposer.Dispose(ref mouse);
            Disposer.Dispose(ref keyboard);
            Disposer.Dispose(ref xbox);
            Disposer.Dispose(ref input);
            GC.SuppressFinalize(this);
        }

    }
}
