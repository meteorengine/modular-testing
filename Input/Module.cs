﻿using System;
using System.ComponentModel.Composition;

namespace MeteorEngine.Input
{
    // Export every method on input module, which can be used in other modules
    [Export(typeof(IInputModule))]
    public class Module : IInputModule
    {
        private InputService _input;
        public bool IsPressingAnyKey => _input.IsPressingAnyKey;

        public void Dispose()
        {
            _input.Dispose();
            _input = null;
            GC.SuppressFinalize(this);
        }

        public bool Initialize()
        {
            _input = new InputService();
            _input.Initialize();
            return true;
        }

        public bool IsAltPressed() => _input.IsAltPressed();
        public bool IsAltPressing() => _input.IsAltPressing();
        public bool IsAltReleased() => _input.IsAltReleased();

        public bool IsButtonPressed(MouseButtons button) => _input.IsButtonPressed(button);
        public bool IsButtonPressing(MouseButtons button) => _input.IsButtonPressing(button);
        public bool IsButtonReleased(MouseButtons button) => _input.IsButtonReleased(button);

        public bool IsControlPressed() => _input.IsControlPressed();
        public bool IsControlPressing() => _input.IsControlPressing();
        public bool IsControlReleased() => _input.IsControlReleased();

        public bool IsKeyPressed(KeyCode key) => _input.IsKeyPressed(key);
        public bool IsKeyPressing(KeyCode key) => _input.IsKeyPressing(key);
        public bool IsKeyReleased(KeyCode key) => _input.IsKeyReleased(key);

        public bool IsShiftPressed() => _input.IsShiftPressed();
        public bool IsShiftPressing() => _input.IsShiftPressing();
        public bool IsShiftReleased() => _input.IsShiftReleased();

        public void Update() => _input.Update();
    }
}
