﻿using MeteorEngine.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeteorEngine.Rendering.DirectX11
{
    internal abstract class Window : IDisposable
    {
        private bool _wasFullscreen;

        protected GraphicsDevice Device;

        public Action InitCallback;

        public Action RunCallback;

        public Action ExitCallback;

        public event EventHandler<EventArgs> Activated;

        public event EventHandler<EventArgs> ClientSizeChanged;

        public event EventHandler<EventArgs> Deactivated;

        public abstract string Title
        {
            get;
            set;
        }

        public abstract bool AllowUserResizing
        {
            get;
            set;
        }

        public abstract Rectangle ClientBounds
        {
            get;
        }

        public abstract bool IsMinimized
        {
            get;
        }

        public abstract bool IsMouseVisible
        {
            get;
            set;
        }

        public abstract bool Focused
        {
            get;
        }

        public abstract System.Windows.Controls.Control NativeWindow
        {
            get;
        }

        public abstract IntPtr Handle
        {
            get;
        }

        public abstract bool Visible
        {
            get;
            set;
        }

        public abstract int Width
        {
            get;
        }

        public abstract int Height
        {
            get;
        }

        public abstract Vector2 MousePosition
        {
            get;
            set;
        }

        public abstract Vector2 MouseScreenPosition
        {
            get;
            set;
        }

        internal abstract bool fullscreen
        {
            set;
        }

        protected Window(GraphicsDevice device)
        {
            Device = device;
            _wasFullscreen = false;
        }

        public abstract void Run();

        public abstract void Resize(int width, int height);

        public abstract bool SetIcon(string path);

        public abstract void Focus();

        public abstract void Exit();

        public abstract void Dispose();

        public void OnActivated(object sender, EventArgs e)
        {
            if (_wasFullscreen)
            {
                Device.Fullscreen = true;
                _wasFullscreen = false;
            }
            Activated?.Invoke(sender, e);
        }

        public void OnDeactivated(object sender, EventArgs e)
        {
            _wasFullscreen = this.Device.Fullscreen;
            Deactivated?.Invoke(sender, e);

        }

        public void OnClientSizeChanged(object sender, EventArgs e)
        {
            ClientSizeChanged?.Invoke(sender, e);
        }
    }
}
