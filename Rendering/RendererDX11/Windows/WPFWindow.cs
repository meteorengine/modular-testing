﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interop;
using MeteorEngine.Mathematics;

namespace MeteorEngine.Rendering.DirectX11
{
    class WPFWindow : Window
    {
        public WPFWindow(GraphicsDevice device) : base(device)
        {
        }

        public override string Title { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override bool AllowUserResizing { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override Rectangle ClientBounds => throw new NotImplementedException();

        public override bool IsMinimized => throw new NotImplementedException();

        public override bool IsMouseVisible { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override bool Focused => throw new NotImplementedException();

        public override Control NativeWindow => throw new NotImplementedException();

        public override IntPtr Handle => throw new NotImplementedException();

        public override bool Visible { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override int Width => throw new NotImplementedException();

        public override int Height => throw new NotImplementedException();

        public override Vector2 MousePosition { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override Vector2 MouseScreenPosition { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        internal override bool fullscreen { set => throw new NotImplementedException(); }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        public override void Exit()
        {
            throw new NotImplementedException();
        }

        public override void Focus()
        {
            throw new NotImplementedException();
        }

        public override void Resize(int width, int height)
        {
            throw new NotImplementedException();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override bool SetIcon(string path)
        {
            throw new NotImplementedException();
        }
    }
}
