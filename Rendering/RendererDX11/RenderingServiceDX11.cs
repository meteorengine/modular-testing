﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Interop;
using D3DFeatureLevel = SharpDX.Direct3D.FeatureLevel;
namespace MeteorEngine.Rendering.DirectX11
{
    //[Export(typeof(IRenderingService))]
    public class RenderingServiceDX11 /*: IRenderingService*/
    {
        //public RendererType Version => RendererType.DirectX_11;
        public string Name => "DirectX 11 Renderer";

        internal GraphicsDevice Device;

       /* public void CreateDevice(RendererType rendererType, D3DImage image, Control container)
        {
            if(Version != rendererType)
            {
                throw new DataLeakException();
            }
            Device = new GraphicsDevice(image,container);
        }*/

        public void Dispose()
        {
        }

        public void Initialize()
        {
        }

        public bool IsSupported()
        {
            //Return if GPU supports target backend and we are on windows
            return Adapter.FindBest().FeatureLevel >= D3DFeatureLevel.Level_11_0 && Environment.OSVersion.Platform == PlatformID.Win32NT;
        }
    }
}
