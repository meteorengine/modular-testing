﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interop;
using DXGIFactory1 = SharpDX.DXGI.Factory1;
using D3D11Device = SharpDX.Direct3D11.Device;
using D3D11DeviceCreationFlags = SharpDX.Direct3D11.DeviceCreationFlags;
namespace MeteorEngine.Rendering.DirectX11
{
    internal class GraphicsDevice
    {
        internal static DXGIFactory1 FactoryDXGI;
        internal D3D11Device Device;
        private Window _window;
        private Adapter _adapter;
        private object _deviceLocker;

        internal GraphicsDevice(D3DImage image, Control container)
        {
            _deviceLocker = new object();
            _adapter = Adapter.FindBest();

            if (image == null)
            {
                _window = new MyNativeWindow(this);
            }
            else
            {
                _window = new WPFWindow(this);
            }
            Device = new D3D11Device(_adapter.AdapterDXGI, D3D11DeviceCreationFlags.BgraSupport);
        }

        public bool Fullscreen { get; internal set; }
    }
}
