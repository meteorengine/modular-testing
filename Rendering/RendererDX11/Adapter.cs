﻿using DXGIAdapter = SharpDX.DXGI.Adapter;
using DXGIOutput = SharpDX.DXGI.Output;
using DXGIAdapterDescription = SharpDX.DXGI.AdapterDescription;
using DXGIFactory1 = SharpDX.DXGI.Factory1;
using D3D11Device = SharpDX.Direct3D11.Device;

using SharpDX.Direct3D;
using System;
using MeteorEngine.Core;
using System.Collections.Generic;

namespace MeteorEngine.Rendering.DirectX11
{
    public class Adapter
    {
        private static Adapter _bestAdapter;
        private static DXGIAdapter[] _adapters;
        private AdapterInfo _info;
        internal DXGIAdapter AdapterDXGI;
        private readonly bool _isPrimary;
        private int _index;
        private DXGIOutput PrimaryOutput;

        public bool IsPrimary => _isPrimary;

        public int Index => _index;

        public bool IsDefaultAdapter => _index == 0;

        public FeatureLevel FeatureLevel => D3D11Device.GetSupportedFeatureLevel(AdapterDXGI);

        private Adapter(DXGIAdapter adapter, bool isPrimary, int index)
        {
            if(index < 0 || adapter == null)
            {
                throw new ArgumentException();
            }

            AdapterDXGI = adapter;
            _index = index;
            _isPrimary = isPrimary;

            if(AdapterDXGI.GetOutputCount() > 0)
            {
                PrimaryOutput = AdapterDXGI.GetOutput(0);
            }
            DXGIAdapterDescription description = AdapterDXGI.Description;
            _info = new AdapterInfo
            {
                Description = description.Description.Trim(),
                DeviceId = description.DeviceId,
                Luid = description.Luid,
                Revision = description.Revision,
                SubsystemId = description.SubsystemId,
                VendorId = description.VendorId,
                SharedSystemMemory = description.SharedSystemMemory,
                DedicatedVideoMemory = description.DedicatedSystemMemory,
                DedicatedSystemMemory = description.DedicatedVideoMemory
            };
        }

        public override string ToString()
        {
            return _info.Description;
        }

        public void Dispose()
        {
            Disposer.Dispose(ref AdapterDXGI);
            PrimaryOutput = null;
            _index = -1;
            GC.SuppressFinalize(this);
        }

        internal static void DisposeAll()
        {
            _adapters = null;
            Disposer.Dispose(ref _bestAdapter);
        }

        internal static Adapter FindBest()
        {
            if(GraphicsDevice.FactoryDXGI == null)
            {
                GraphicsDevice.FactoryDXGI = new DXGIFactory1();
            }

            if(_bestAdapter == null)
            {
                _adapters = GraphicsDevice.FactoryDXGI.Adapters;
                if(_adapters == null || _adapters.Length == 0)
                {
                    throw new NotSupportedException("No DXGI adapters found!");
                }

                if(_adapters.Length == 1)
                {
                    _bestAdapter = new Adapter(_adapters[0], true, 0);
                }
                else
                {
                    List<DXGIAdapter> adapters = new List<DXGIAdapter>(_adapters);
                    adapters.Sort(sortAdapters);
                    if(adapters.Count == 0)
                    {
                        throw new NotSupportedException("No DXGI adapters after ranking!");
                    }
                    _bestAdapter = new Adapter(adapters[0], adapters[0] == _adapters[0], getAdapterIndex(adapters[0]));
                }
            }
            return _bestAdapter;
        }

        private static int getAdapterIndex(DXGIAdapter adapter)
        {
            for (int i = 0; i < _adapters.Length; i++)
            {
                if(_adapters[i] == adapter)
                {
                    return i;
                }
            }
            throw new DataLeakException();
        }

        private static int sortAdapters(DXGIAdapter x, DXGIAdapter y)
        {
            //TODO: probably add some priority Nvidia, AMD > Intel... and check if is hardware or if it's software emulated adapter

            //Get descriptions
            DXGIAdapterDescription desc_left = x.Description;
            DXGIAdapterDescription desc_right = y.Description;

            //Get adapter feature levels
            FeatureLevel sfl_left = D3D11Device.GetSupportedFeatureLevel(x);
            FeatureLevel sfl_right = D3D11Device.GetSupportedFeatureLevel(y);

            //If they have different feature level
            if(sfl_left != sfl_right)
            {
                if(sfl_left > sfl_right) { return -1; } else { return 1; }
            }
            else
            {
                //If they have the same, for the availible memory size
                long ssml = desc_left.SharedSystemMemory;
                long ssmr = desc_right.SharedSystemMemory;

                if(ssml == ssmr)
                {
                    return getAdapterIndex(x).CompareTo(getAdapterIndex(y));
                }

                if (ssml > ssmr) { return -1; } else { return 1; }
            }
        }
    }
}
