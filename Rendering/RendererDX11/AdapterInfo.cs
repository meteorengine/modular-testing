﻿
namespace MeteorEngine.Rendering.DirectX11
{
    public struct AdapterInfo
    {
        public string Description;
        public int DeviceId;
        public long Luid;
        public int Revision;
        public int SubsystemId;
        public int VendorId;
        public long DedicatedSystemMemory;
        public long DedicatedVideoMemory;
        public long SharedSystemMemory;

        public bool IsAMD
        {
            get
            {
                return VendorId == 4098;
            }
        }

        public bool IsIntel
        {
            get
            {
                return VendorId == 32902;
            }
        }

        public bool IsNVidia
        {
            get
            {
                return VendorId == 4318;
            }
        }
    }
}
