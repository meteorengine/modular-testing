﻿using MeteorEngine.Core;
using System;
using System.ComponentModel.Composition;
using System.IO;

namespace Core
{
    [Export(typeof(ICoreService))]
    public class CoreService : ICoreService
    {
        public string Name  => "Core";

        public void Dispose()
        {
        }

        public void Initialize()
        {
        }

        public void InitializeCore(MyGame GameClass, CoreParameters Parameters, Action OnFinalPrepare)
        {
            SetupPath(Parameters);
        }

        private void SetupPath(CoreParameters parameters)
        {
           /* _editorMode = parameters.EditorMode;
            _gameMode = parameters.GameMode;
            _temporaryFolderName = parameters.TemporaryFolderName;
            TemporaryFolderPath = Path.Combine(EngineStartupPath, _temporaryFolderName) + "\\";*/
        }
    }
}
