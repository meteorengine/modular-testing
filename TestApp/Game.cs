﻿using MeteorEngine;
using MeteorEngine.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    internal sealed class Game : MyGame
    {
        private Assembly EngineAssembly;

        internal Game(string[] args) : base(60, args)
        {
            EngineAssembly = GetType().Assembly;
            InitializeCore();
            


            CreateDevice();
        }


        private void InitializeCore()
        {
            Core = ModuleManager.Get<ICoreService>();
            Core.InitializeCore(this, new CoreParameters { EditorMode = false, GameMode = false, TemporaryFolderName = ".engine" }, OnFinalPrepare);
        }

        private void OnFinalPrepare()
        {
        }
    }
}
